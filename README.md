# ALIEN

This is a fork of the [Z80-CP/M and Alien Disk Format Reader] created 1992
by Volker Dose and [Egbert Schroeer] for TCS Genie IIIs with Holte-CP/M 3.

Assembling of ALIEN was tested with:

  * MACRO-80 Assembler `M80.COM`
  * [Pasmo] - multiplatform Z80 cross-assembler by Julián Albo
  * [RASM] - powerful Z80 assembler by Edouard Berge
  * Z80 assembler `Z80.COM` by Eberhard Mattes
  * Z80 assembler `Z80ASM.COM` by SLR Systems
  * [Z80Asm] - A Z80 Assembler in Python by Memotech Bill
  * [zasm] - Z80 / 8080 / Z180 Assembler by Günter Woigk
  * [zmac] - public domain Z80 macro cross-assembler by Bruce Norskog and others
  * [ZSM4] - Z80/Z180/Z280 Macro Assembler for CP/M, UZI180 and RSX180 by Hector Peraza

## List of implemented alien formats

| Format    | Description                                | Tracks | Density | Capacity |  SSZ | Skew |
|:----------|:-------------------------------------------|:------:|:-------:|---------:|-----:|:----:|
| ABC-80    | Luxor ABC 80                               |   40   |  SS-DD  |   152 KB |  256 |    7 |
| ABC-800   | Luxor ABC 800                              |   80   |  DS-DD  |   632 KB |  256 |      |
| ACTRIX    | Actrix/Access Matrix                       |   40   |  SS-DD  |   171 KB |  512 |    3 |
| ALPHA-P2  | Triumph Adler alphaTronic P2               |   40   |  SS-DD  |   320 KB |  256 |      |
| ALPHA-P3  | Triumph Adler alphaTronic P3               |   80   |  DS-DD  |   790 KB | 1024 |      |
| ALPHA-PC  | Triumph Adler alphaTronic PC               |   40   |  DS-DD  |   320 KB |  256 |      |
| ALSPA     | Alspa / Dimension 1010 / Heurikon Z1.4SSG  |   77   |  SS-DD  |   600 KB | 1024 |      |
| ALTOS     | Altos Series 5 & 580                       |   80   |  DS-DD  |   708 KB |  512 |      |
| AMPRO     | Ampro Little Board                         |   40   |  SS-DD  |   190 KB |  512 |      |
| ASTER     | Aster CT-80 System                         |   80   |  DS-DD  |   702 KB |  256 |      |
| ASTER-2   | Aster CT-80 System #2                      |   80   |  DS-DD  |   790 KB |  512 |      |
| ASTER-3S  | Aster CT-80 System #3                      |   80   |  DS-DD  |   780 KB | 1024 |      |
| BEEHIVE   | Beehive Topper II                          |   40   |  DS-DD  |   380 KB |  512 |      |
| BOND-2    | Bondwell 2                                 |   80   |  SS-DD  |   350 KB |  256 |      |
| BOND-12   | Bondwell 12                                |   40   |  SS-DD  |   170 KB |  256 |      |
| BULLET    | Wave Mate Bullet                           |   80   |  DS-DD  |   790 KB | 1024 |      |
| CASIO     | Casio FP-1100/FP-1020                      |   40   |  DS-DD  |   320 KB |  256 |      |
| CDOS      | Cromemco CDOS                              |   40   |  SS-SD  |    83 KB |  128 |    5 |
| CDOS-40   | Cromemco CDOS                              |   40   |  DS-DD  |   390 KB |  512 |    4 |
| CDOS-77   | Cromemco CDOS                              |   77   |  DS-DD  |   1.2 MB |  512 |   11 |
| DR-DSDD   | Digital Research 8" CP/M Standard          |   77   |  DS-DD  |   988 KB |  256 |      |
| EAGLE     | Eagle I/II                                 |   80   |  SS-DD  |   390 KB | 1024 |    2 |
| ELZET     | Elzet 80 Portable / Kneiser & Doering 3000 |   80   |  DS-DD  |   780 KB | 1024 |      |
| EXECUTIVE | Osborne 2 Executive                        |   40   |  SS-DD  |   185 KB | 1024 |      |
| EXIDY     | Exidy Sorcerer                             |   77   |  SS-DD  |   302 KB |  256 |    5 |
| EXO       | EXO Systems Corporation                    |   77   |  SS-DD  |   600 KB |  512 |      |
| FELTRON   | Feltron COMPULADY                          |   77   |  DS-DD  |   1.1 MB |  512 |    5 |
| FRITZ     | Fritz Chwolka                              |   80   |  DS-DD  |   1.2 MB |  512 |      |
| FUJITSU   | Fujitsu 8/16                               |   80   |  SS-DD  |   316 KB |  512 |      |
| HEATH-89  | Heath H89, Magnolia CP/M                   |   40   |  SS-DD  |   166 KB |  512 |      |
| HOLBORN   | Holborn 6100/6500                          |   77   |  DS-DD  |   1.2 MB | 1024 |      |
| HOLTE-2S  | Thomas Holte Speedmaster CP/M 2.2a System  |   80   |  SS-DD  |   390 KB |  512 |      |
| HOLTE-3S  | Thomas Holte TCS Genie IIIs CP/M 3.0b      |   80   |  DS-DD  |   790 KB |  512 |      |
| HOLTE-G3  | Thomas Holte EACA Genie III CP/M 2.2c/3.0a |   80   |  DS-DD  |   790 KB |  512 |      |
| IBM-3740  | IBM 3740 (Digital Research CP/M Standard)  |   77   |  SS-SD  |   244 KB |  128 |    6 |
| JONOS     | Jonos Escort                               |   70   |  SS-DD  |   306 KB |  512 |      |
| JOYCE     | Amstrad PCW 8256 "Joyce"                   |   40   |  SS-DD  |   175 KB |  512 |      |
| JUNIOR    | Junior 80                                  |   80   |  DS-DD  |   702 KB |  512 |    2 |
| KAYPRO    | Kaypro II                                  |   40   |  SS-DD  |   196 KB |  512 |      |
| KK-D80    | Klaus Kaempf CP/M 2.2x/3.0 Data            |   80   |  DS-DD  |   800 KB | 1024 |      |
| KK-S80    | Klaus Kaempf CP/M 2.2x/3.0 System          |   80   |  DS-DD  |   768 KB | 1024 |      |
| LNW-256   | LNW Research LNW80                         |   40   |  SS-DD  |   166 KB |  256 |    5 |
| LNW-512   | LNW Research LNW80                         |   40   |  SS-DD  |   180 KB |  512 |      |
| LOBO-256  | Lobo MAX-80                                |   40   |  SS-DD  |   160 KB |  256 |      |
| LOBO-512  | Lobo MAX-80                                |   40   |  SS-DD  |   185 KB |  512 |      |
| LOBO-82   | Lobo MAX-80 8" CP/M 2.2                    |   77   |  SS-DD  |   562 KB |  256 |      |
| LOBO-83   | Lobo MAX-80 8" CP/M 3.0                    |   77   |  SS-DD  |   636 KB |  512 |      |
| LOWE-A1   | Lowe Electronics CP/M 2.2a                 |   80   |  SS-DD  |   346 KB |  256 |      |
| LOWE-A2   | Lowe Electronics CP/M 2.2a                 |   80   |  DS-DD  |   696 KB |  256 |      |
| LOWE-B1   | Lowe Electronics CP/M 2.2b (4K Block)      |   80   |  SS-DD  |   384 KB |  256 |      |
| LOWE-B2   | Lowe Electronics CP/M 2.2b (4K Block)      |   80   |  DS-DD  |   696 KB |  256 |      |
| MAGIC     | Magic PBC-88                               |   40   |  DS-DD  |   390 KB |  512 |      |
| MC-68K    | MC-68K                                     |   80   |  DS-DD  |   780 KB | 1024 |      |
| MC-CPM    | MC-CPM                                     |   80   |  DS-DD  |   768 KB | 1024 |      |
| MD2       | Morrow Micro Decision MD2                  |   40   |  SS-DD  |   190 KB | 1024 |    3 |
| MD3       | Morrow Micro Decision MD3                  |   40   |  DS-DD  |   390 KB | 1024 |    3 |
| MEGABYTE  | Megabyte                                   |   77   |  DS-DD  |   1.4 MB | 1024 |      |
| MICRO-K64 | Micro-K 64                                 |   80   |  DS-DD  |   780 KB | 1024 |      |
| MM-D40    | Montezuma Micro 40T Standard DATA          |   40   |  SS-DD  |   200 KB |  512 |    2 |
| MM-S40    | Montezuma Micro 40T Standard SYSTEM        |   40   |  SS-DD  |   170 KB |  256 |    2 |
| MM-D80    | Montezuma Micro 80T DS DATA                |   80   |  DS-DD  |   800 KB |  512 |    2 |
| MM-S80    | Montezuma Micro 80T DS SYSTEM              |   80   |  DS-DD  |   710 KB |  256 |    2 |
| MOLEC-S9  | Molecular Series 9                         |   40   |  DS-DD  |   342 KB |  512 |      |
| MONROE    | Monroe OC 8820                             |   80   |  SS-DD  |   308 KB |  256 |    4 |
| MOPPEL    | Modulares Prozessor-Programm der ELO       |   80   |  SS-DD  |   304 KB |  256 |      |
| NABU-1100 | Nabu 1100                                  |   77   |  DS-DD  |   988 KB |  256 |    7 |
| NABU-PC   | Nabu PC                                    |   40   |  SS-DD  |   200 KB | 1024 |    2 |
| NASCOM-C  | Nascom C XDOS                              |   80   |  DS-DD  |   790 KB |  512 |      |
| NEC-8800  | NEC PC-8800/8801                           |   40   |  DS-DD  |   306 KB |  256 |      |
| NIXDORF   | Nixdorf 8810/30                            |   80   |  DS-DD  |   770 KB |  512 |      |
| OMIKRON   | Omikron Mapper I TRS-80 Model I/III        |   35   |  SS-SD  |    83 KB |  128 |    4 |
| OSBORNE   | Osborne 1                                  |   40   |  SS-SD  |    90 KB |  256 |    2 |
| OSB-MEGA  | Osborne ECG Megadisk                       |   80   |  DS-DD  |   790 KB | 1024 |      |
| OR-512    | Oettle & Reichler                          |   80   |  DS-DD  |   702 KB |  512 |    3 |
| OR-1024   | Oettle & Reichler                          |   80   |  DS-DD  |   780 KB | 1024 |      |
| ORION-V   | dy-4 Systems, Inc. Orion-V                 |   77   |  SS-DD  |   486 KB |  256 |    9 |
| P2000     | Philips P2000                              |   80   |  DS-DD  |   632 KB |  256 |    2 |
| P3500     | Philips P3500 TurboDOS                     |   80   |  DS-DD  |   800 KB | 1024 |    2 |
| P3800     | Philips P3800 TurboDOS                     |   80   |  DS-DD  |   632 KB |  256 |      |
| PANASONIC | Panasonic JD-850                           |   77   |  DS-DD  |   988 KB |  256 |    6 |
| PCPM-40   | Personal CP/M-86 v2                        |   40   |  DS-DD  |   360 KB |  512 |      |
| PCPM-80   | Personal CP/M-86 v2                        |   80   |  DS-DD  |   720 KB |  512 |      |
| PERTEC    | Pertec PCC-2000                            |   77   |  SS-DD  |   480 KB |  256 |    5 |
| PMC-101   | PMC-101 MicroMate                          |   40   |  DS-DD  |   390 KB | 1024 |      |
| PROF-3    | Conitec Prof-80 #3                         |   80   |  SS-DD  |   386 KB | 1024 |      |
| PROF-4    | Conitec Prof-80 #4                         |   80   |  DS-DD  |   770 KB |  512 |    2 |
| PROF-8    | Conitec Prof-180 #8                        |   77   |  DS-DD  |   1.1 MB |  512 |    4 |
| PROF-14   | Conitec Prof-181 #14                       |   80   |  DS-DD  |   1.4 MB | 1024 |    2 |
| QUARK-100 | Megatel Quark 100                          |   35   |  SS-DD  |   180 KB |  512 |    2 |
| QX10-256  | Epson QX-10                                |   40   |  DS-DD  |   290 KB |  256 |      |
| QX10-512  | Epson QX-10                                |   40   |  DS-DD  |   380 KB |  512 |      |
| QX10-1024 | Epson QX-10                                |   40   |  DS-DD  |   400 KB | 1024 |      |
| RAINBOW   | DEC Rainbow 100+                           |   80   |  SS-DD  |   390 KB |  512 |    2 |
| RAIR-40   | RAIR Supermicro                            |   40   |  DS-DD  |   241 KB |  128 |    4 |
| RAIR-80   | RAIR Supermicro                            |   80   |  DS-DD  |   782 KB |  512 |      |
| REH-780   | Tilmann Reh CPU280 CP/M-3                  |   80   |  DS-DD  |   780 KB | 1024 |    2 |
| REH-1440  | Tilmann Reh CPU280 CP/M-3                  |   80   |  DS-DD  |   1.4 MB | 1024 |    2 |
| REH-1760  | Tilmann Reh CPU280 CP/M-3                  |   80   |  DS-DD  |   1.7 MB | 1024 |    2 |
| ROBIN     | DEC VT-180 "Robin"                         |   40   |  SS-DD  |   171 KB |  512 |    2 |
| ROBO-D40  | Robotron PC 1715 Data                      |   40   |  SS-DD  |   190 KB | 1024 |      |
| ROBO-S40  | Robotron PC 1715 System                    |   40   |  SS-DD  |   190 KB | 1024 |      |
| ROBO-D80  | Robotron PC 1715 Data                      |   80   |  DS-DD  |   780 KB | 1024 |      |
| ROBO-S80  | Robotron PC 1715 System                    |   80   |  DS-DD  |   780 KB | 1024 |      |
| RS-CPM3   | Radio Shack TRS-80 Model 4/4P CP/M Plus    |   40   |  SS-DD  |   156 KB |  512 |      |
| RTS-80    | Dobert & Bitsch RTS-80                     |   80   |  SS-DD  |   312 KB |  256 |      |
| SAGE-IV   | Sage Computer Technology                   |   80   |  DS-DD  |   632 KB |  512 |      |
| SANYO-40  | Sanyo MBC-1000/1100                        |   40   |  DS-DD  |   312 KB |  256 |    3 |
| SANYO-80  | Sanyo MBC-200/1200/1250                    |   80   |  DS-DD  |   624 KB |  256 |    3 |
| SB-80     | Colonial Data SB-80                        |   77   |  DS-DD  |   1.2 MB | 1024 |    3 |
| SB-80-DR  | Colonial Data SB-80 (BIOS by Dave Rand)    |   77   |  DS-DD  |   1.2 MB | 1024 |    3 |
| SCHMIDTKE | Schmidtke Genie I CP/M 2.2 System          |   80   |  DS-DD  |   768 KB | 1024 |      |
| SCHROEDER | Gerald Schroeder Genie IIs CP/M 2.2 System |   80   |  DS-DD  |   768 KB | 1024 |      |
| SISYPHUS  | Sisyphus                                   |   77   |  SS-DD  |   450 KB |  128 |      |
| SOABAR    | Soabar CDX 31                              |   40   |  DS-DD  |   390 KB | 1024 |      |
| SONY-70   | Sony SMC-70                                |   70   |  SS-DD  |   268 KB |  256 |    3 |
| TAYLORIX  | Taylorix System 4                          |   77   |  DS-DD  |   988 KB |  256 |    6 |
| TELEVIDEO | TeleVideo 8nn-series                       |   40   |  DS-DD  |   342 KB |  256 |      |
| TRS-LB    | TRS-80 Model II Lifeboat                   |   77   |  SS-DD  |   600 KB | 1024 |    3 |
| TRS-FMG   | TRS-80 Model II FMG                        |   77   |  SS-DD  |   486 KB |  256 |      |
| TRS-PT    | TRS-80 Model II/12/16 Pickles & Trout      |   77   |  SS-DD  |   600 KB |  512 |      |
| TURBODOS  | TurboDOS                                   |   80   |  DS-DD  |   780 KB | 1024 |      |
| VISUAL    | Visual Technology 1050                     |   80   |  SS-DD  |   390 KB |  512 |      |
| VIXEN     | Osborne 4 Vixen                            |   40   |  DS-DD  |   390 KB | 1024 |    2 |
| VORTEX    | Amstrad CPC Vortex                         |   80   |  DS-DD  |   712 KB |  512 |      |
| WAVEMATE  | Wave-Mate                                  |   77   |  DS-DD  |   1.2 MB |  512 |      |
| X820-I    | Xerox 820                                  |   40   |  SS-SD  |    82 KB |  128 |    5 |
| X820-II   | Xerox 820-II                               |   40   |  SS-DD  |   157 KB |  256 |      |
| Z-100     | Zenith Z-100/Heath H-100                   |   40   |  DS-DD  |   320 KB |  512 |      |
| ZORBA     | Telcon Industries Zorba                    |   40   |  DS-DD  |   390 KB |  512 |      |

[Egbert Schroeer]: https://github.com/Egbert-Azure/
[Pasmo]: https://pasmo.speccy.org/
[RASM]: https://github.com/EdouardBERGE/rasm/
[Z80-CP/M and Alien Disk Format Reader]: https://github.com/Egbert-Azure/Z80-CP-M-format-reader/
[Z80Asm]: https://github.com/Memotech-Bill/Z80Asm/
[zasm]: https://k1.spdns.de/Develop/Projects/zasm/
[zmac]: https://gitlab.com/jengun/zmac/
[ZSM4]: https://github.com/hperaza/ZSM4/
